﻿using System;
using System.Windows.Forms;


namespace shoppeform4._0
{
    public partial class Form1 : Form
    {

        Product Coke = new Product(1.75m, "Coke", 8, "Pop");
        Product Sprite = new Product(1.75m, "Sprite", 8, "Pop");
        Product RootBeer = new Product(1.75m, "RootBeer", 8, "Pop");
        Product Lays = new Product(1.75m, "Lays", 8, "Chips");
        Product Kettle = new Product(1.75m, "Kettle", 8, "Chips");
        Product Doritos = new Product(1.75m, "Doritos", 8, "Chips");
        Product Ham = new Product(1.75m, "Ham", 8, "Sandwich");
        Product Turkey = new Product(1.75m, "Turkey", 8, "Sandwich");
        Product Club = new Product(1.75m, "Club", 8, "Sandwich");
        Product Gourmet = new Product(1.75m, "Gourmet", 8, "Coffee");
        Product Cappuccino = new Product(1.75m, "Cappuccino", 8, "Coffee");
        Product Espresso = new Product(1.75m, "Espresso", 8, "Coffee");

        public Form1()
        {
            InitializeComponent();
            listBox1.Items.Add(Coke.Name);
            listBox1.Items.Add(Sprite.Name);
            listBox1.Items.Add(RootBeer.Name);
            listBox1.Items.Add(Lays.Name);
            listBox1.Items.Add(Kettle.Name);
            listBox1.Items.Add(Doritos.Name);
            listBox1.Items.Add(Ham.Name);
            listBox1.Items.Add(Turkey.Name);
            listBox1.Items.Add(Club.Name);
            listBox1.Items.Add(Gourmet.Name);
            listBox1.Items.Add(Cappuccino.Name);
            listBox1.Items.Add(Espresso.Name);
        }
       
        private void addPop_Click(object sender, EventArgs e)
        {
            try
            {  
                lbxMoneyList.Items.Add(listBox1.SelectedItem);
                GetSelectedTotal();
            }
            catch (ArgumentNullException)
            {
                Total.Text.ToString();
            }
           finally
            {
               GetSelectedTotal();
            }

        }

        private void GetSelectedTotal()
        {
            decimal tempValue = 0.0m;
            foreach (object p in lbxMoneyList.Items)
            {
                System.Diagnostics.Debug.WriteLine("The pukjgprice is " + p);

                if (p is Product tempProduct)
                {
                    System.Diagnostics.Debug.WriteLine("The pukjgprice is " + tempProduct.Price);

                    tempValue += tempProduct.Price;

                }
             
               
            }
            Total.Text = tempValue.ToString();

        }

        private void Total_TextChanged(object sender, EventArgs e)
        {

        }

        private void lbxMoneyList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Remove_Click(object sender, EventArgs e)
        {
            lbxMoneyList.Items.Remove(listBox1.SelectedItem);
            GetSelectedTotal();
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            lbxMoneyList.Items.Clear();
            GetSelectedTotal();

        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
