﻿using System;
using MoneyLib;

namespace shoppeform4._0
{
    public interface IProduct
    {
        Decimal Price { get; set; }
        String Name { get; set; }
        int Volume { get; set; }
        String FoodType { get; set; }
        String ToString();
    }


    public class Product : IProduct
    {
        public Decimal Price { get; set; }
        public String Name { get; set; }
        public int Volume { get; set; }
        public String FoodType { get; set; }

        public Product(decimal newPrice, String newName, int newVolume, String newFoodType)
        {
            System.Diagnostics.Debug.WriteLine("The price is " + newPrice);
            Price = newPrice;
            System.Diagnostics.Debug.WriteLine("The price is " + Price);
            Name = newName;
            Volume = newVolume;
            FoodType = newFoodType;
        }
    }

    //public class Pop : Product
    //{
    //    public Pop(string popName) : base(popName, 12, new Money(new Currency("USD"), 1.75m))
    //    {

    //    }

    //}

    //public class Coke : Pop
    //public class Coke : IProduct
    //{
    //    Money price;
    //    public Money Price { get { return this.price; } set { this.price = value; } }

    //    String name;
    //    public string Name { get { return this.name; } set { this.name = "Not empty"; } }

    //    int volume;
    //    public int Volume { get { return this.volume; } set { this.volume = value; } }

    //    String foodType;
    //    public string FoodType { get { return this.foodType; } set { this.foodType = value; } }

    //    public override String ToString()
    //    {
    //        return Name + ": " + Price.Value;
    //    }
    //}


    //    //



    //    //public class Sprite : Pop
    //    public class Sprite : IProduct
    //{
    //    Money price;
    //    String name;
    //    int volume;
    //    String foodType;
    //    public Money Price { get{ return this.price; } set { this.price = value; } }
    //    public string Name { get{ return this.name; } set { this.name = value;} }
    //    public int Volume { get { return this.volume; } set { this.volume = value; } }
    //    public string FoodType { get { return this.foodType; } set { this.foodType = value; } }

    //    public override String ToString(){
    //        return Name + ": " + Price.Value;
    //    }
    //}

    ////public class RootBeer : Pop
    //public class RootBeer: IProduct
    //{
    //    Money price;
    //    String name;
    //    int volume;
    //    String foodType;
    //    public Money Price { get{return price;} set { price = new Money(new Currency("USD"), 1.75m); } }
    //    public string Name { get{return name;} set{name = "Root Beer";} }
    //    public int Volume { get { return volume; } set { volume = 24; } }
    //    public string FoodType { get { return foodType; } set { foodType = "Pop"; } }

    //    public override String ToString(){
    //        return Name + ": " + Price.Value;
    //    }
    //}

    ////public class Chips : Product
    ////{
    ////    public Chips(String chipName, Money chipPrice) : base(chipName, 16, chipPrice)
    ////    {

    ////    }
    ////}

    ////public class Lays : Chips
    //public class Lays : IProduct
    //{
    //    Money price;
    //    String name;
    //    int volume;
    //    String foodType;
    //    public Money Price { get{return Price;} set { Price = new Money(new Currency("USD"), 1.25m); } }
    //    public string Name { get{return Name;} set{Name = "Lays";} }
    //    public int Volume { get { return Volume; } set { Volume = 20; } }
    //    public string FoodType { get { return FoodType; } set { FoodType = "Chips"; } }

    //    public override String ToString(){
    //        return Name + ": " + Price.Value;
    //    }
    //}

    ////public class Doritos : Chips
    //public class Doritos : IProduct
    //{
    //    Money price;
    //    String name;
    //    int volume;
    //    String foodType;
    //    public Money Price { get{return Price;} set { Price = new Money(new Currency("USD"), 1.25m); } }
    //    public string Name { get{return Name;} set{Name = "Doritos";} }
    //    public int Volume { get { return Volume; } set { Volume = 20; } }
    //    public string FoodType { get { return FoodType; } set { FoodType = "Chips"; } }

    //    public override String ToString(){
    //        return Name + ": " + Price.Value;
    //    }
    //}

    ////public class Kettle : Chips
    //public class Kettle : IProduct
    //{
    //    Money price;
    //    String name;
    //    int volume;
    //    String foodType;
    //    public Money Price { get{ return Price; } set { Price = new Money(new Currency("USD"), 1.50m); } }
    //    public string Name { get{ return Name; } set{Name = "Kettle";} }
    //    public int Volume { get { return Volume; } set { Volume = 5; } }
    //    public string FoodType { get { return FoodType; } set { FoodType = "Chips"; } }

    //    public override String ToString(){
    //        return Name + ": " + Price.Value;
    //    }
    //}

    ////public class Sandwich : Product
    ////{
    ////    public Sandwich(String sandName, Money sandPrice) : base(sandName, 6, sandPrice)
    ////    {

    ////    }
    ////}

    ////public class Ham : Sandwich
    //public class Ham : IProduct
    //{
    //    Money price;
    //    String name;
    //    int volume;
    //    String foodType;
    //    public Money Price { get{return Price;} set { Price = new Money(new Currency("USD"), 1.75m); } }
    //    public string Name { get{return Name;} set{Name = "Ham";} }
    //    public int Volume { get { return Volume; } set { Volume = 77; } }
    //    public string FoodType { get { return FoodType; } set { FoodType = "Sandwich"; } }

    //    public override String ToString(){
    //        return Name + ": " + Price.Value;
    //    }
    //}

    ////public class Turkey : Sandwich
    //public class Turkey : IProduct
    //{
    //    Money price;
    //    String name;
    //    int volume;
    //    String foodType;
    //    public Money Price { get{return Price;} set { Price = new Money(new Currency("USD"), 1.75m); } }
    //    public string Name { get{return Name;} set{Name = "Turkey";} }
    //    public int Volume { get { return Volume; } set { Volume = 45; } }
    //    public string FoodType { get { return FoodType; } set { FoodType = "Sandwich"; } }

    //    public override String ToString(){
    //        return Name + ": " + Price.Value;
    //    }
    //}

    ////public class Club : Sandwich
    //public class Club : IProduct
    //{
    //    Money price;
    //    String name;
    //    int volume;
    //    String foodType;
    //    public Money Price { get{return Price;} set { Price = new Money(new Currency("USD"), 1.75m); } }
    //    public string Name { get{return Name;} set{Name = "Club";} }
    //    public int Volume { get { return Volume; } set { Volume = 43; } }
    //    public string FoodType { get { return FoodType; } set { FoodType = "Sandwich"; } }

    //    public override String ToString(){
    //        return Name + ": " + Price.Value;
    //    }
    //}

    ////public class Coffee : Product
    ////{
    ////    public Coffee(String coffeeName, Money coffeePrice) : base(coffeeName, 20, coffeePrice)
    ////    {

    ////    }
    ////}

    ////public class Gormet : Coffee
    //public class Gourmet : IProduct
    //{
    //    Money price;
    //    String name;
    //    int volume;
    //    String foodType;
    //    public Money Price { get{return Price;} set { Price = new Money(new Currency("USD"), 2.99m); } }
    //    public string Name { get{return Name;} set{Name = "Gourmet";} }
    //    public int Volume { get { return Volume; } set { Volume = 12; } }
    //    public string FoodType { get { return FoodType; } set { FoodType = "Coffee"; } }

    //    public override String ToString(){
    //        return Name + ": " + Price.Value;
    //    }
    //}

    ////public class Cappuchino : Coffee
    //public class Cappuccino : IProduct
    //{
    //    Money price;
    //    String name;
    //    int volume;
    //    String foodType;
    //    public Money Price { get { return Price; } set { Price = new Money(new Currency("USD"), 3.99m); } }
    //    public string Name { get { return Name; } set { Name = "Cappuccino"; } }
    //    public int Volume { get { return Volume; } set { Volume = 54; } }
    //    public string FoodType { get { return FoodType; } set { FoodType = "Coffee"; } }

    //    public override String ToString()
    //    {
    //        return Name + ": " + Price.Value;
    //    }
    //}

    ////public class Espresso : Coffee
    //public class Espresso : IProduct
    //{
    //    Money price;
    //    String name;
    //    int volume;
    //    String foodType;
    //    public Money Price { get { return price; } set { price = new Money(new Currency("USD"), 3.49m); } }
    //    public string Name { get { return name; } set { name = "Espresso"; } }
    //    public int Volume { get { return volume; } set { volume = 9; } }
    //    public string FoodType { get { return foodType; } set { foodType = "Coffee"; } }

    //    public override String ToString()
    //    {
    //        return Name + ": " + Price.Value;
    //    }
    //}
}

